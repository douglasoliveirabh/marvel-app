﻿using MarvelApp.Domain.History;
using MarvelApp.Domain.Marvel;
using MarvelApp.Shared.Application;
using MarvelApp.Shared.Application.Marvel;
using MarvelApp.Shared.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MarvelApp.Application
{
    public class SearchAppService : ISearchAppService
    {
        private readonly ISearchHistoryRepository _searchHistoryRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICharacterClient _characterClient;

        public SearchAppService(ISearchHistoryRepository searchHistoryRepository,
                                ICharacterClient characterClient,
                                IUnitOfWork unitOfWork)
        {
            _searchHistoryRepository = searchHistoryRepository;
            _unitOfWork = unitOfWork;
            _characterClient = characterClient;
        }

        public Task<IEnumerable<SearchHistory>> GetAllOrderedBySearchDateAsync()
        {
            return _searchHistoryRepository.GetAllOrderedBySearchDateAsync();
        }

        public Task<MarvelApiResult<Character[]>> GetCharactersByNameAsync(string name)
        {
            _searchHistoryRepository.InsertAsync(new SearchHistory(name));
            _unitOfWork.CommitAsync();
            return this._characterClient.GetCharactersByNameAsync(name);
        }        
    }
}
