﻿using AutoMapper;
using MarvelApp.Application.ViewModels;
using MarvelApp.Domain.History;
using MarvelApp.Domain.Marvel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarvelApp.Application.AutoMapper
{
    public class DomainToViewModelProfile : Profile
    {
        public DomainToViewModelProfile()
        {
            CreateMap<Character, CharacterViewModel>()
                .ForMember(dest => dest.ThumbnailUrl,
                           opt => opt.MapFrom(src => src.Thumbnail != null ? 
                                                     $"{src.Thumbnail.Path}.{src.Thumbnail.Extension}" :
                                                     ""))
                 .ForMember(dest => dest.Stories, opt => opt.MapFrom(src => src.Stories.Items));

            CreateMap<SearchHistory, SearchHistoryViewModel>();
        }
    }
}
