﻿using MarvelApp.Domain.Marvel;
using MarvelApp.Shared.Application.Marvel;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Refit;
using System;
using System.Threading.Tasks;

namespace MarvelApp.Application.Marvel
{
    public class CharacterClient : ICharacterClient
    {
        private readonly IMarvelClient _marvelClient;
        private readonly ICharacterApi _characterApi;

        public CharacterClient(IMarvelClient marvelClient)
        {
            _marvelClient = marvelClient;
            _characterApi = RestService.For<ICharacterApi>(_marvelClient.Host, new RefitSettings
            {
                JsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                }
            });
        }

        public Task<MarvelApiResult<Character[]>> GetCharactersByNameAsync(string name)
        {
            return _characterApi.GetCharactersByNameAsync(name, this._marvelClient.GetTimeStamp(), this._marvelClient.GetApiKey(), this._marvelClient.GetHash());
        }

        public Task<MarvelApiResult<Character[]>> GetCharactersByNameStartsWithAsync(string name)
        {
            return _characterApi.GetCharactersByNameStartsWithAsync(name, this._marvelClient.GetTimeStamp(), this._marvelClient.GetApiKey(), this._marvelClient.GetHash());
        }
    }
}
