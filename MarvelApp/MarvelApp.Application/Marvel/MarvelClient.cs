﻿using MarvelApp.Shared.Application.Marvel;
using System;
using System.Security.Cryptography;
using System.Text;

namespace MarvelApp.Application.Marvel
{
    public class MarvelClient : IMarvelClient
    {
        public string PublicKey { get; private set; }
        public string PrivateKey { get; private set; }
        public string Host { get; private set; }

        private string _timeStamp;

        public MarvelClient(string host, string publicKey, string privateKey)
        {
            PublicKey = publicKey;
            PrivateKey = privateKey;
            Host = host;
            _timeStamp = DateTime.Now.Ticks.ToString();
        }

        public string GetHash()
        {
            byte[] bytes = Encoding.UTF8.GetBytes($"{this._timeStamp}{PrivateKey}{PublicKey}");
            byte[] bytesHash = MD5.Create().ComputeHash(bytes);
            return BitConverter.ToString(bytesHash).ToLower().Replace("-", String.Empty);
        }

        public string GetApiKey()
        {
            return this.PublicKey;
        }

        public string GetTimeStamp() {
            return this._timeStamp;
        }
    }
}
