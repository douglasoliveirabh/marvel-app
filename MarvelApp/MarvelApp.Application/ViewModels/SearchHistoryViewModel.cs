﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarvelApp.Application.ViewModels
{
    public class SearchHistoryViewModel
    {
        public string SearchTerm { get; set; }
        public DateTime SearchDate { get; set; }
    }
}
