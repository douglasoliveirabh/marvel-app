﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarvelApp.Application.ViewModels
{
    public class SearchCharacterViewModel
    {
        public string Term { get; set; }

        public List<CharacterViewModel> SearchResults { get; set; }
    }
}
