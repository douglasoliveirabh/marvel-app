﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarvelApp.Application.ViewModels
{
    public class StoryViewModel
    {
        public string Name { get; set; }
        public string Type { get; set; }
    }
}
