﻿using MarvelApp.Domain.Marvel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarvelApp.Application.ViewModels
{
    public class CharacterViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Modified { get; set; }
        public string ThumbnailUrl { get; set; }
        public IEnumerable<Story> Stories { get; set; }
}
}
