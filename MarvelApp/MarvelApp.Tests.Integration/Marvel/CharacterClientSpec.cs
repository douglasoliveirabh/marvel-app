﻿using FluentAssertions;
using MarvelApp.Application.Marvel;
using MarvelApp.Shared.Application.Marvel;
using System;
using Xunit;

namespace MarvelApp.Tests.Integration.Marvel
{
    public class CharacterClientSpec
    {
        private readonly ICharacterClient characterClient;

        public CharacterClientSpec()
        {
            var marvelClient = new MarvelClient("http://gateway.marvel.com", "41f99730b2a205ae281ad8a4a4b720b2", "1e24cfbb4e315f55a6a305592eeda8b06ee37b90");
            this.characterClient = new CharacterClient(marvelClient);
        }

        [Fact(DisplayName = "Should test character that exists")]
        [Trait("CharacterClientSpec", "")]
        public async void ShouldTestCharacterThatExists()
        {
            var result = await this.characterClient.GetCharactersByNameAsync("Captain America");
            result.Data.Results.Should().HaveCountGreaterThan(0);
        }

        [Fact(DisplayName = "Should test character that doesn't exists")]
        [Trait("CharacterClientSpec", "")]
        public async void ShouldTestCharacterThatDoesntExists()
        {
            var result = await this.characterClient.GetCharactersByNameAsync("Volverine");
            result.Data.Results.Should().HaveCount(0);
        }
    }
}
