﻿using AutoMapper;
using MarvelApp.Application.ViewModels;
using MarvelApp.Domain.History;
using MarvelApp.Infra.Identity.Models;
using MarvelApp.Shared.Application;
using MarvelApp.Shared.Application.Marvel;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Marvel.Web.Mvc.Controllers
{
    public class HomeController : Controller
    {        
        private readonly IMapper _mapper;
        private ISearchAppService _searchAppService;

        public HomeController(IMapper mapper,
                              ISearchAppService searchAppService)
        {            
            _mapper = mapper;
            _searchAppService = searchAppService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public  async Task<IActionResult> SearchCharacter(SearchCharacterViewModel viewModel)
        {
            var characters = await this._searchAppService.GetCharactersByNameAsync(viewModel.Term);           
            viewModel.SearchResults = this._mapper.Map<List<CharacterViewModel>>(characters.Data.Results);            
            return View("Index", viewModel);
        }       

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
