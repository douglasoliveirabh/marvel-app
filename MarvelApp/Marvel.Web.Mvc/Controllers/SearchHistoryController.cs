﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MarvelApp.Application.ViewModels;
using MarvelApp.Shared.Application;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Marvel.Web.Mvc.Controllers
{
    [Authorize]
    public class SearchHistoryController : Controller
    {
        private readonly ISearchAppService _searchHistoryAppService;
        private readonly IMapper _mapper;

        public SearchHistoryController(ISearchAppService searchHistoryAppService, IMapper mapper)
        {

            _searchHistoryAppService = searchHistoryAppService;
            _mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var searches = await _searchHistoryAppService.GetAllOrderedBySearchDateAsync();           
            return View(_mapper.Map<List<SearchHistoryViewModel>>(searches));
        }
    }
}
