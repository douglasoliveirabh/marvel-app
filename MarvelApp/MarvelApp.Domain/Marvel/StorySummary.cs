﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarvelApp.Domain.Marvel
{
    public class StorySummary
    {
        public IEnumerable<Story> Items { get; set; }

    }
}
