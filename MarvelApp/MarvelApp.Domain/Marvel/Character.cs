﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarvelApp.Domain.Marvel
{
    public class Character
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Modified { get; set; }
        public Thumbnail Thumbnail { get; set; }   
        public StorySummary Stories { get; set; }
    }
}
