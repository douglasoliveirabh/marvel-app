﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarvelApp.Domain.Marvel
{
    public class Story
    {        
        public string Name { get; set; }
        public string Type { get; set; }
    }
}
