﻿namespace MarvelApp.Domain.Marvel
{
    public class Thumbnail
    {
        public string Path { get; set; }
        public string Extension { get; set; }
    }
}