﻿using System.ComponentModel.DataAnnotations;

namespace MarvelApp.Infra.Identity.Models.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
