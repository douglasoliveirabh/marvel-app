using System.ComponentModel.DataAnnotations;

namespace MarvelApp.Infra.Identity.Models.AccountViewModels
{
    public class ExternalLoginViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
