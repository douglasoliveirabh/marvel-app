﻿using FluentAssertions;
using MarvelApp.Application.Marvel;
using Xunit;

namespace MarvelApp.Tests.Unit.Application
{
    public class MarvelClientSpec
    {

        [Fact(DisplayName = "Should test if apiKey generated is correct")]
        [Trait("MarvelClientSpec", "")]
        public void ShouldTestIfApiKeyGeneratedIsCorrect()
        {
            var marvelClient = new MarvelClient("http://gateway.marvel.com", "1234", "abcd");

            marvelClient.GetHash().Should().Equals("ffd275c5130566a2916217b101f26150");
            marvelClient.GetApiKey().Should().Equals("1234");
        }

        [Fact(DisplayName = "Should test if hash generated is correct")]
        [Trait("MarvelClientSpec", "")]
        public void ShouldTestIfHashGeneratedIsCorrect()
        {
            var marvelClient = new MarvelClient("http://gateway.marvel.com", "1234", "abcd");
            marvelClient.GetHash().Should().Equals("ffd275c5130566a2916217b101f26150");            
        }

        [Fact(DisplayName = "Should test if host is equals than passed by parameter")]
        [Trait("MarvelClientSpec", "")]
        public void ShouldTestIfHostIsEqualsThanPassedByParameter()
        {
            var marvelClient = new MarvelClient("http://gateway.marvel.com", "1234", "abcd");
            marvelClient.Host.Should().Equals("http://gateway.marvel.com");
        }




    }
}
