﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarvelApp.Shared.Application.Marvel
{
    public interface IMarvelClient
    {
        string Host { get; }

        string PublicKey { get; }
        string PrivateKey { get; }

        string GetHash();
        string GetApiKey();
        string GetTimeStamp();
    }
}
