﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarvelApp.Shared.Application.Marvel.Result
{
    public class Data<TResult>
    {
        public int OffSet { get; set; }
        public int Limit { get; set; }
        public int Total { get; set; }
        public int Count { get; set; }
        public TResult Results { get; set; }
    }
}
