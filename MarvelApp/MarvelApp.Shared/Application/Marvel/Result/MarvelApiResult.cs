﻿using MarvelApp.Shared.Application.Marvel.Result;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarvelApp.Shared.Application.Marvel
{
    public class MarvelApiResult<TResult>
    {
        public Data<TResult> Data { get; set; }
    }
}
