﻿using MarvelApp.Domain.Marvel;
using Refit;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MarvelApp.Shared.Application.Marvel
{
    public interface ICharacterApi
    {
        [Get("/v1/public/characters?name={name}&ts={timestamp}&apikey={apikey}&hash={hash}")]
        [Headers("Accept: application/json")]
        Task<MarvelApiResult<Character[]>> GetCharactersByNameAsync(string name, string timestamp, string apikey, string hash);

        [Get("/v1/public/characters?nameStartsWith={name}&ts={timestamp}&apikey={apikey}&hash={hash}")]
        [Headers("Accept: application/json")]
        Task<MarvelApiResult<Character[]>> GetCharactersByNameStartsWithAsync(string name, string timestamp, string apikey, string hash);        
    }
}
