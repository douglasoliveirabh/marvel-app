﻿using MarvelApp.Domain.Marvel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MarvelApp.Shared.Application.Marvel
{
    public interface ICharacterClient
    {
        Task<MarvelApiResult<Character[]>> GetCharactersByNameAsync(string name);
        Task<MarvelApiResult<Character[]>> GetCharactersByNameStartsWithAsync(string name);
    }
}
