﻿using MarvelApp.Domain.History;
using MarvelApp.Domain.Marvel;
using MarvelApp.Shared.Application.Marvel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MarvelApp.Shared.Application
{
    public interface ISearchAppService 
    {
        Task<IEnumerable<SearchHistory>> GetAllOrderedBySearchDateAsync();        
        Task<MarvelApiResult<Character[]>> GetCharactersByNameAsync(string name);
    }
}
