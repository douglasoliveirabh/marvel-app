﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MarvelApp.Shared.Repositories
{
    public interface IReadRepository<TEntity>
    {
        Task<IEnumerable<TEntity>> FindAsync(Expression<Func<TEntity, bool>> condition);

        Task<IEnumerable<TEntity>> GetAllAsync();
    }
}
