﻿using MarvelApp.Domain.History;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MarvelApp.Shared.Repositories
{
    public interface ISearchHistoryRepository : IReadRepository<SearchHistory>, 
                                                IWriteRepository<SearchHistory>
    {
        Task<IEnumerable<SearchHistory>> GetAllOrderedBySearchDateAsync();
    }
}
