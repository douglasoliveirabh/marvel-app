﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MarvelApp.Shared.Repositories
{
    public interface IUnitOfWork : IDisposable
    {        
        Task<int> CommitAsync();
    }
}
