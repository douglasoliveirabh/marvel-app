﻿using MarvelApp.Domain.History;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarvelApp.Infra.Data.Mapping
{
    public class SearchHistoryMapping : IEntityTypeConfiguration<SearchHistory>
    {
        public void Configure(EntityTypeBuilder<SearchHistory> builder)
        {
            builder
                .HasKey(x => x.Id);

            builder
                .Property(x => x.SearchTerm)                
                .HasMaxLength(250)
                .IsRequired();

            builder
                .Property(x => x.SearchDate)                
                .IsRequired();
        }
    }
}
