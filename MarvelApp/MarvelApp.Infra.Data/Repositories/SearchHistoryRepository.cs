﻿using MarvelApp.Domain.History;
using MarvelApp.Infra.Data.Context;
using MarvelApp.Shared.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarvelApp.Infra.Data.Repositories
{
    public class SearchHistoryRepository : BaseRepository<SearchHistory>, ISearchHistoryRepository
    {
        public SearchHistoryRepository(MarvelAppContext dbContext) : base(dbContext)
        {
        }

        public Task<IEnumerable<SearchHistory>> GetAllOrderedBySearchDateAsync()
        {
            return Task.FromResult(DbSet.AsQueryable().OrderByDescending(x => x.SearchDate).AsEnumerable());
        }
    }
}
