﻿using MarvelApp.Infra.Data.Mapping;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarvelApp.Infra.Data.Context
{
    public class MarvelAppContext : DbContext
    {
        public MarvelAppContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new SearchHistoryMapping());
        }
    }
}
