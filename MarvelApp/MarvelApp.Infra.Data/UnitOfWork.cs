﻿using MarvelApp.Infra.Data.Context;
using MarvelApp.Shared.Repositories;
using System.Threading.Tasks;

namespace MarvelApp.Infra.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        MarvelAppContext _context;

        public UnitOfWork(MarvelAppContext context)
        {
            _context = context;
        }

        public Task<int> CommitAsync()
        {
            return _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
