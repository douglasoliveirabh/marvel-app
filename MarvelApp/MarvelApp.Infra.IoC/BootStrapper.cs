﻿using MarvelApp.Infra.IoC.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarvelApp.Infra.IoC
{
    public class BootStrapper
    {
        public static void RegisterServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddIdentity(configuration);
            services.AddMarvelApi(configuration);
            services.AddMarvelAppRepositories(configuration);
            services.AddAppServices();
        }
    }
}
