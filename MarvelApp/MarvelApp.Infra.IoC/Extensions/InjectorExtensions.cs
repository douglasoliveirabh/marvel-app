﻿using MarvelApp.Application;
using MarvelApp.Application.Marvel;
using MarvelApp.Infra.Data;
using MarvelApp.Infra.Data.Context;
using MarvelApp.Infra.Data.Repositories;
using MarvelApp.Infra.Identity.Context;
using MarvelApp.Infra.Identity.Models;
using MarvelApp.Infra.Identity.Services;
using MarvelApp.Shared.Application;
using MarvelApp.Shared.Application.Marvel;
using MarvelApp.Shared.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace MarvelApp.Infra.IoC.Extensions
{
    public static class InjectorExtensions
    {

        public static void AddIdentity(this IServiceCollection services, IConfiguration config)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(config.GetConnectionString("DefaultConnection"), b => b.MigrationsAssembly("Marvel.Infra.Data.Migrations")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddTransient<IEmailSender, EmailSender>();
        }

        public static void AddMarvelApi(this IServiceCollection services, IConfiguration config)
        {

            var marvelHost = config.GetSection("MarvelApi:Host").Value;
            var marvelPublicKey = config.GetSection("MarvelApi:PublicKey").Value;
            var marvelPrivateKey = config.GetSection("MarvelApi:PrivateKey").Value; ;

            services.AddTransient<ICharacterClient, CharacterClient>();
            services.AddTransient<IMarvelClient>(x => new MarvelClient(marvelHost, marvelPublicKey, marvelPrivateKey));

        }


        public static void AddMarvelAppRepositories(this IServiceCollection services, IConfiguration config)
        {
            services
                .AddDbContext<MarvelAppContext>(options => options.UseSqlServer(config.GetConnectionString("DefaultConnection"), b => b.MigrationsAssembly("Marvel.Infra.Data.Migrations")));

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddScoped<ISearchHistoryRepository, SearchHistoryRepository>();
            
        }

        public static void AddAppServices(this IServiceCollection services)
        {
            services.AddScoped<ISearchAppService, SearchAppService>();
        }


    }
}
